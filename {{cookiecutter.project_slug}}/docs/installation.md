(installation)=
# Installation

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of {{ cookiecutter.project_slug }}!
```

To install the _{{ cookiecutter.project_slug }}_ package, we
recommend that you install it from PyPi via

```bash
pip install {{ cookiecutter.project_slug }}
```

Or install it directly from [the source code repository on
Gitlab][source code repository]
via:

```bash
pip install git+https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git
```

The latter should however only be done if you want to access the
development versions.

[source code repository]: https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}

(install-develop)=
## Installation for development

Please head over to our
`contributing guide <contributing>`{.interpreted-text role="ref"} for
installation instruction for development.
