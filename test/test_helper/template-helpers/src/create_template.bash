# create_template
# ===============
#
# Summary: Create the template with a certain configuration
#
# Usage: create_template <config>

# Options:
#   --no-commit  If set, do not make a commit of the created package
#   <config>    The JSON-encoded configuration that you want to test
#
# IO:
#   STDERR - the failed expression, on failure
# Globals:
#   none
# Returns:
#   0 - if the template has been created successfully
#   1 - otherwise
#
#   ```bash
#   @test 'create()' {
#     create_template '{"some_param": "test"}'
#   }
#   ```
#

# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

create_template() {
  local -i commit_repo=1

  while (( $# > 0 )); do
    case "$1" in
    --no-commit) commit_repo=0; shift ;;
    --) shift; break ;;
    *) break ;;
    esac
  done

  echo "default_context: $1" > "$BATS_TEST_TMPDIR/config.yaml"
  cookiecutter --no-input --config-file "$BATS_TEST_TMPDIR/config.yaml" . --output-dir $BATS_TEST_TMPDIR
  if (( commit_repo == 1 )); then
    git -C ${PROJECT_FOLDER} commit --no-gpg-sign -m "Initial commit"
  fi
}
