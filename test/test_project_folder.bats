# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "test using a project folder (project_folder)" {
    # first create a standard package
    create_template
    echo "config.yaml" >> ${PROJECT_FOLDER}/.gitignore
    git -C ${PROJECT_FOLDER} add .gitignore

    # then create one inside a child directory
    BATS_TEST_TMPDIR=${PROJECT_FOLDER} create_template '{project_folder: ".", project_slug: "python"}'
    export PROJECT_FOLDER=${PROJECT_FOLDER}/python
    setup_venv
    make -C ${PROJECT_FOLDER} lint
}
