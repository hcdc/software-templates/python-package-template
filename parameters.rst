.. SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

Configuration Parameters
========================

Here we describe the individual parameters for the ``cookiecutter.json``
configuration file and the usage of environment variables.

.. contents::

Cookiecutter Config
-------------------

.. documentation on the parameters from cookiecutter.json

.. list-table:: Parameters
   :header-rows: 1

   * - Parameter
     - Description
     - Default
     - Format
   * - ``project_authors``
     - Full names of the author(s)
     - ``Firstname Lastname, Another-Firstname Lastname``
     - Full name in the format ``Firstname Lastname``.
       In case of multiple authors, they need to be comma-separated.

       .. important::

          The number of authors must match the number of emails in
          ``project_author_emails``!

   * - ``project_author_emails``
     - Email address of the author(s)
     - ``someone@hereon.de, someone.else@hereon.de``
     - Raw email as ``username@domain.de``.
       In case of multiple authors, they need to be comma-separated.

       .. important::

          The number of emails must match the number of authors in
          ``project_authors``

   * - ``project_maintainers``
     - Full names of the maintainer(s)
     - Fallback to ``project_authors``
     - Same as for ``project_authors``.

       .. important::
          The number of maintainers must match the number of emails in
          ``project_maintainer_emails``!
   * - ``project_maintainer_emails``
     - Email address of the maintainer(s)
     - Fallback to ``project_author_emails``
     - Same as for ``project_authors``.

       .. important::

          The number of emails must match the number of maintainers in
          ``project_maintainer_emails``!
   * - ``gitlab_host``
     - Fully qualified domain of the Gitlab Server.
     - ``codebase.helmholtz.cloud``
     - A valid domain name
   * - ``gitlab_username``
     - The user or group name on GitLab. Can also be the full path to a subgroup
     - ``hcdc``
     - a single slug or multiple slugs separated by ``/``
   * - ``git_remote_protocoll``
     - Whether to register the remote as ``https`` or ``ssh`` after setting up
       the project locally. Does not have an effect on the documentation.
     - ``https``
     - One of ``https`` or ``ssh``
   * - ``institution``
     - Name of the institution that should be acknowledged
     - ``Helmholtz-Zentrum Hereon``
     - string
   * - ``institution_url``
     - Public website of the institution
     - ``https://www.hereon.de``
     - A url
   * - ``copyright_holder``
     - Body that has the copyright on the package.
     - Fallback to ``institution``
     - string
   * - ``copyright_year``
     - Year or year range of the copyright
     - the current year
     - a year (e.g. ``2013``) or a year range (e.g. ``2021-2023``)
   * - ``use_reuse``
     - whether to use the `reuse tool`_ or not. If not, you can ignore the
       ``code_license``, ``documentation_license`` and
       ``supplementary_files_license``

       If you choose yes, we highly recommend that you register your
       repository at https://api.reuse.software/
     - ``yes``
     - ``yes`` or ``no``
   * - ``code_license``
     - SPDX license identifier for the code (aka python files).
       *Can be ignored if use_reuse is no*.

       .. important::

          Please clarify this with the head of your institute! Changing
          licenses afterwards can be very difficult.

     - ``EUPL-1.2``
     - A valid SPDX-identifier, see https://spdx.org/licenses/
   * - ``documentation_license``
     - SPDX license identifier for the documentation.
       *Can be ignored if use_reuse is no*.

       .. important::

          Please clarify this with the head of your institute! Changing
          licenses afterwards can be very difficult.

     - ``CC-BY-4.0``
     - A valid SPDX-identifier, see https://spdx.org/licenses/
   * - ``supplementary_files_license``
     - SPDX license identifier for technical files such as ``.gitignore`` or
       python package configuration.
       *Can be ignored if use_reuse is no*.

       .. important::

          Please clarify this with the head of your institute! Changing
          licenses afterwards can be very difficult.
     - ``CC0-1.0``
     - A valid SPDX-identifier, see https://spdx.org/licenses/
   * - ``project_title``
     - Short title for the project
     - ``My Python Package``
     - string, can contain spaces
   * - ``project_slug``
     - Slug of the project on GitLab
     - Fallback to ``project_title`` but with ``-`` instead of spaces
     - string without spaces or slashes (should match the following pattern
       ``^[-a-zA-Z0-9_]+\Z``)
   * - ``package_folder``
     - folder name for the python module
     - Fallback to ``project_slug`` but with ``_`` instead of ``-``
     - a valid python module name
   * - ``project_short_description``
     - Optional description for the package
     - Fallback to project title
     - arbitrary string
   * - ``project_folder``
     - A folder where to create the files. Use this together with the
       `--output-dir` option for `cruft`, if the template is not supposed to
       live at the root of the git repository.
     - *empty string*
     - a folder relative to the root of the git repository
   * - ``keywords``
     - keywords for the project
     - *empty string*
     - comma-separated list of strings. Each string should represent one
       keyword
   * - ``documentation_url``
     - URL where the documentation will be hosted
     - A URL on readthedocs.io
     - a valid URL, should end with a trailing ``/``
   * - ``use_markdown_for_documentation``
     - Whether to add the ``myst_parser`` extension for the sphinx
       documentation and use markdown files instead of restructured text.
     - ``yes``
     - ``yes`` or ``no``
   * - ``ci_build_stage``
     - Should there be a separate build stage for building the package prior
       to running the tests?
     - ``no``
     - ``no`` or ``yes``
   * - ``use_cibuildwheel``
     - If your however package only uses standard python code, set this to
       ``no``. If you have code written in C, C++, Cython, etc. you should
       use cibuildwheel_ during the build. cibuildwheel_ builds the wheel for
       the package on all distros (linux, windows and macos) and all python
       versions (3.9, 3.10, 3.11 and 3.12). This makes it much more easier to
       install the package on different systems.

       You can see a working implementation of this with Cython in the
       repository at `hcdc/software-templates/demos/gitlab-cibuildwheel-demo <https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo>`__

       .. important::

            The CI is configured such that it runs on the MacOS, Windows and
            Linux Gitlab-CI Runners of the Helmholtz codebase Gitlab. If you
            are using another gitlab, please adapt the ci config at
            ``ci/.gitlab-ci.build.yml`` accordingly.
     - ``no``
     - ``no`` or ``yes``
   * - ``ci_matrix``
     - How should the CI-matrix be implemented? You can choose between no
       matrix at all (which will just install the project and run the tests),
       and a ``pipenv`` based matrix different ``Pipfile`` spin up different
       scenarios
     - ``no``
     - ``no`` or ``pipenv``
   * - ``use_pytest_xdist``
     - Should we use pytest-xdist in the continuous integration to distribute \
       the unit tests to multiple parallel processes?
     - ``no``
     - ``no`` or ``yes``
   * - ``deploy_package_in_ci``
     - Whether or not to build the python package and make it available in the
       gitlab package registry for the project.
     - ``yes``
     - ``yes`` or ``no``
   * - ``deploy_pages_in_ci``
     - Whether or not to deploy the documentation with GitLab Pages. If you
       choose ``yes``, it will be stored to a as artifact via a deploy job
       (suitable if you deploy it via GitLab pages), if you choose
       ``git-push``, the build will be pushed to a dedicated ``gh-pages``
       branch (suitable if you deploy it git GitHub Pages)
     - ``yes``
     - ``yes``, ``no`` or ``git-push``


.. _cibuildwheel: https://cibuildwheel.readthedocs.io/en/stable/


Environment variables
---------------------
Some environment variables determine how the repository is setup. If you use
this project together with cruft, these variables will not be stored in
the `.cruft.json` file.

.. list-table:: Environment variables
   :header-rows: 1

   * - Environment variable
     - Description
     - Default
     - Format
   * - ``SILENT_HOOKS``
     - Whether or not the entire process should be silent. Set this to any
       value to disable any output by the ``post_gen_project`` hook.
     - *Empty string*
     - Any value
   * - ``PARENT_GIT_REPO``
     - **Deprecated.** Please use the ``project_folder`` option instead.
       **Note:** setting ``PARENT_GIT_REPO`` also involves setting
       ``SKIP_PRE_COMMIT``
     - *Empty string*
     - **DEPRECATED**
   * - ``SKIP_PRE_COMMIT``
     - Whether or not to run ``pre-commit run`` at the end of the project
       creation. This template runs ``pre-commit`` run at the end of the
       project generation to make some formatting and linting. If you want to
       skip this step, set this variable to any value.
     - *Empty string*
     - Any value
