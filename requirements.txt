# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

cruft>=2.13.0
GitPython
pre-commit
reuse-shortcuts>=1.0.1
